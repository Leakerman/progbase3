#ifndef USER_H
#define USER_H

#include <string>
#include <QMetaType>
#include <QString>
#include <QByteArray>

struct User
{
    int id;
    QString username;
    QString password_hash;
    QByteArray avatar;

};
Q_DECLARE_METATYPE(User)

#endif // USER_H
