#-------------------------------------------------
#
# Project created by QtCreator 2019-04-27T21:31:51
#
#-------------------------------------------------


QT += multimedia multimediawidgets
QT       += core gui
QT += network xml

TARGET = server_game
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#TARGET = CPP_QT_Game_Tutorial
#TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    bullet.cpp \
    enemy.cpp \
    game.cpp \
    player.cpp \
    win.cpp \
    lose.cpp \
    mainwindow.cpp \
    mytcpserver.cpp \
    serversettings.cpp\
    menu.cpp

HEADERS += \
    bullet.h \
    enemy.h \
    game.h \
    player.h \
    win.h \
    lose.h \
    gamesettings.h \
serversettings.h\
    mainwindow.h \
    mytcpserver.h \
    menu.h


FORMS += \
    win.ui \
    lose.ui \
    mainwindow.ui \
serversettings.ui\
    menu.ui

RESOURCES += \
    new_qrc.qrc

