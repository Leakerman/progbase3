#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include <QSqlDatabase>

#include "storage.h"


class SqliteStorage: public Storage
{
    const string dir_name_;
    QSqlDatabase db_;

public:
    SqliteStorage(const string & dir_name);

     bool open() ;
     bool close() ;
    // Images
     vector<Image> getAllImages(void) ;
     vector<Image> getAllUserImages(int user_id);
     vector<Image> getAllFiltratedUserImages(int user_id, Filter filter, int current_page = 1, int page_size = 7);
     optional<Image> getImageById(int image_id) ;
     bool updateImage(const Image &image) ;
     bool removeImage(int image_id) ;
     int insertImage(const Image &image) ;
    // Packs
     vector<Pack> getAllPacks(void) ;
     optional<Pack> getPackById(int pack_id) ;
     bool updatePack(const Pack &pack) ;
     bool removePack(int pack_id) ;
     int insertPack(const Pack &pack) ;
     // users
     QVariant authUser(QString const & username, QString const & password);
     optional<User> getUserById(int user_id);
     int insertUser(const QString &username,const QString &password);
     void updateUser(int userId,const QString &new_username, const QString &new_password, QByteArray & new_avatar);
     bool getUserByUsername(const QString &username);
     // links
     vector<Pack> getAllImagePacks(int image_id);
     bool insertImagePacks(int image_id, int pack_id);
     bool removeImagePacks(int image_id, int pack_id);
};

#endif // SQLITE_STORAGE_H
