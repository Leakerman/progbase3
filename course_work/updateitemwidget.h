#ifndef UPDATEITEMWIDGET_H
#define UPDATEITEMWIDGET_H

#include <QDialog>
#include "../myLibrary/mylibrary.h"
#include <QTimer>
#include <QLabel>
#include <QMovie>
#include <QMediaPlaylist>
#include <QMediaPlayer>

namespace Ui {
class updateitemwidget;
}

class updateitemwidget : public QDialog
{
    Q_OBJECT

public:
    explicit updateitemwidget(QWidget *parent = 0, Storage * storage = nullptr,Image * changingImage = nullptr);
    ~updateitemwidget();

    Storage * storage;
    Image  changingImage_;
    string data;
    QTimer * timer;
    QMovie * movie;

    bool create = false;
    bool update = false;
    bool imageNotChanged = true;

    void refresh();

public slots:
    void closeEvent(QCloseEvent *event);
    void movelabel();
    void addMovie();

private slots:

    void on_create_2_clicked();

    void on_change_clicked();

    void on_addItemToList_clicked();

    void on_expand_clicked();

    void on_addPack_clicked();

    void on_deletePack_clicked();

    void on_pashalka_clicked();

private:
    Ui::updateitemwidget *ui;
};

#endif // UPDATEITEMWIDGET_H
