#include "updateitemwidget.h"
#include "ui_updateitemwidget.h"

#include "creatingimagewidget.h"
#include "serversettings.h"
#include "packsbrowser.h"
#include <QDebug>
#include <QLineEdit>
#include <QString>
#include <QListWidget>

updateitemwidget::updateitemwidget(QWidget *parent, Storage * storage , Image * changingImage) :
    QDialog(parent),
    ui(new Ui::updateitemwidget)
{
    this->storage = storage;

    Image image;
    image.id = changingImage->id;
    image.Name = changingImage->Name;
    image.Data = changingImage->Data;
    image.width = changingImage->width;
    image.height = changingImage->height;
    changingImage_ = image;

    ui->setupUi(this);

    this->timer = new QTimer();
    this->timer->singleShot(6000,this,SLOT(addMovie()));
    this->timer->start();
}

updateitemwidget::~updateitemwidget()
{
    delete ui;
}

void updateitemwidget::on_create_2_clicked()
{
    this->create = true;
    imageNotChanged = false;
    ui->btnsize_2->close();
    ui->heigth_2->close();
    ui->width_2->close();

    creatingimagewidget  w(&data ,"",this , this->storage ,ui->btnsize_2->value() , ui->width_2->value() , ui->heigth_2->value() );
    w.setWindowTitle("Creating new image");
    w.exec();
    ui->create_2->setEnabled(false);
    ui->change->setEnabled(false);

}

void updateitemwidget::on_change_clicked()
{
    this->update = true;
    imageNotChanged = false;
    ui->btnsize_2->close();
    ui->heigth_2->close();
    ui->width_2->close();

    creatingimagewidget  w(&data ,changingImage_.Data,this , this->storage ,ui->btnsize_2->value() , changingImage_.width , changingImage_.height );
    w.setWindowTitle("Changing image");
    w.exec();
    ui->create_2->setEnabled(false);
    ui->change->setEnabled(false);
}

void updateitemwidget::on_addItemToList_clicked()
{
    Image image;

    image.id = changingImage_.id;
    image.Name = ui->Name->text().toStdString();


    if(create)
    {
        image.Data = this->data;
        image.height = ui->heigth_2->value();
        image.width = ui->width_2->value();
    }else if(update)
    {
        image.Data = this->data;
        image.height = changingImage_.height;
        image.width = changingImage_.width;
    }else if(imageNotChanged)
    {
        image.Data = changingImage_.Data;
        image.height = changingImage_.height;
        image.width = changingImage_.width;
    }

    this->storage->updateImage(image);
    this->close();
    this->parentWidget()->show();
}

void updateitemwidget::on_expand_clicked()
{
   this->setFixedSize(this->width() + 200, this->height() );
   ui->expand->close();

   ui->packsList->move(340,5);
   ui->packsList->setFixedSize(200, this->height() - 40);


   ui->addPack->move(340,this->height() - 30);
   ui->addPack->setFixedSize(100, ui->addPack->height() );
   ui->deletePack->move(440,this->height() - 30);
   ui->deletePack->setFixedSize(100,ui->deletePack->height());

   refresh();
}

void updateitemwidget::refresh()
{
    ui->packsList ->clear();
    std::vector<Pack >  entity = storage->getAllImagePacks(this->changingImage_.id);
    foreach(auto pack , entity)
    {
        QVariant qvar;
        qvar.setValue(pack);
        QListWidgetItem * item = new QListWidgetItem(  );
        item->setText( QString::fromStdString(pack.Name) );
        item->setData(Qt::UserRole, qvar);
        ui->packsList->addItem(item);
    }
}

void updateitemwidget::closeEvent(QCloseEvent *event)
{
    this->parentWidget()->show();
}


void updateitemwidget::on_addPack_clicked()
{
    packsBrowser browser( this->changingImage_.id, this->storage,this);
    browser.setWindowTitle("add category");
    browser.exec();
    refresh();
}

void updateitemwidget::on_deletePack_clicked()
{
    QList<QListWidgetItem *> list = ui->packsList->selectedItems();
    if (list.count() > 0)
    {
      QListWidgetItem * item = list.at(0);
      int itemRow = ui->packsList->row(item);

      QVariant qvar;
      qvar = item->data(Qt::UserRole);
      Pack pack = qvar.value<Pack>();

      this->storage->removeImagePacks(this->changingImage_.id,pack.id);
    }
    refresh();
}

void updateitemwidget::on_pashalka_clicked()
{

    serversettings w(this);
    w.exec();

}

void updateitemwidget::movelabel()
{
    if(ui->movieLabel->x() + ui->movieLabel->width() < 0)
    {
        delete this->timer;
        delete this->movie;
        ui->pashalka->move(10,200);
    }

    ui->movieLabel->move( ui->movieLabel->pos().rx() -= 4  , ui->movieLabel->y() );

}

void updateitemwidget::addMovie()
{
    this->timer->setInterval(125);
    connect(this->timer,SIGNAL(timeout()),this,SLOT(movelabel()));

    this->movie = new QMovie(":/gif/sources/milos.gif");

    ui->movieLabel->move(220,110);
    ui->movieLabel->setFixedSize(141,131);

    ui->movieLabel->setMovie(movie);
    movie->setScaledSize(QSize(ui->movieLabel->width(), ui->movieLabel->height()));
    movie->start();

}

