#include "lose.h"
#include "ui_lose.h"
#include "game.h"
#include "menu.h"

#include <QLabel>
#include <QMovie>
#include <QDebug>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QGraphicsView>
#include "menu.h"

//extern QGraphicsView * view;
extern QMediaPlayer * music;
extern gameSettings * settings;
extern Game * game;
//extern int score;

lose::lose(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::lose)
{
    ui->setupUi(this);
    QMovie *movie = new QMovie(":/data/images/lose.gif");
    if (movie->isValid() == false) {
        qDebug() << "Error";
        abort();
    }
    ui->label_2->setMovie(movie);
    movie->setScaledSize(QSize(this->width(), this->height()));
    movie->start();
    QMediaPlaylist * playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl("qrc:/data/sounds/lose.mp3"));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);

    QMediaPlayer * local_music  = new QMediaPlayer();
    local_music->setPlaylist(playlist);
    local_music->play();
//    game->close();
    music->stop();
    game->view->close();
    this->window()->setWindowTitle("YOU LOSE");
}

lose::~lose()
{
    delete ui;
}



void lose::closeEvent(QCloseEvent *event)
{
    this->hide();
    menu m;
    m.exec();

}

void lose::on_main_menu_clicked()
{
    this->hide();
    menu m;
    m.exec();
}
