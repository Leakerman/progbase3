#ifndef FILTER_H
#define FILTER_H

#include <QDialog>
#include "filtersettings.h"

namespace Ui {
class filter;
}

class filterwidget : public QDialog
{
    Q_OBJECT

public:
    explicit filterwidget(QWidget *parent = 0);
    ~filterwidget();
    Filter filter;
private slots:

    void closeEvent(QCloseEvent *event);

    void on_pushButton_clicked();

    void on_nameASC_clicked();

    void on_nameDESC_clicked();

    void on_sizeASC_clicked();

    void on_sizeDESC_clicked();

private:
    Ui::filter *ui;
};

#endif // FILTER_H
