#include "mylibrary.h"

#include "sqlite_storage.h"
#include <QtSql>
#include <string>
#include <QMessageBox>


SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_);
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected){return false;}

    return true;
}

bool SqliteStorage::close()
{
    db_.close();
    return false;
}

// Images
vector<Image> SqliteStorage::getAllImages(void)
{

    vector<Image> image;

    QSqlQuery query("SELECT * FROM images");
    while (query.next())
    {
       int id =  query.value("id").toInt();
       string name = query.value("name").toString().toStdString();
       string data = query.value("data").toString().toStdString();
       int width =  query.value("width").toInt();
       int heigth =  query.value("height").toInt();

       Image i;
       i.id = id;
       i.Name = name;
       i.Data = data;
       i.width = width;
       i.height = heigth;

       image.push_back(i);
    }

    return image;
}
optional<Image> SqliteStorage::getImageById(int image_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM images WHERE id = :id");
    query.bindValue(":id", image_id);
    if (!query.exec()) {
       qDebug() << "get image error:" << query.lastError();
       return nullopt;
    }

    if (query.next()) {
        int id =  query.value("id").toInt();
        string name = query.value("name").toString().toStdString();
        string data = query.value("data").toString().toStdString();
        int width =  query.value("width").toInt();
        int heigth =  query.value("height").toInt();

        Image i;
        i.id = id;
        i.Name = name;
        i.Data = data;
        i.width = width;
        i.height = heigth;

        return i;
    } else {
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updateImage(const Image &image)
{
    QSqlQuery query;
    query.prepare("UPDATE images SET name = :name, data = :data, width = :width, height = :height WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(image.Name));
    query.bindValue(":data", QString::fromStdString(image.Data));
    query.bindValue(":width", image.width);
    query.bindValue(":height", image.height);
    query.bindValue(":id", image.id);
    if (!query.exec()){
        qDebug() << "updatePerson error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }

    return true;
}
bool SqliteStorage::removeImage(int image_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM images WHERE id = :id");
    query.bindValue(":id", image_id);
    if (!query.exec()){
        qDebug() << "deletePerson error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertImage(const Image &image)
{


    QSqlQuery query;
    query.prepare("INSERT INTO images (name, data, width, height, user_id) VALUES (:name, :data, :width, :height, :user_id)");
    query.bindValue(":name", QString::fromStdString(image.Name));
    query.bindValue(":data", QString::fromStdString(image.Data));
    query.bindValue(":width", image.width);
    query.bindValue(":user_id", image.user_id);
    query.bindValue(":height", image.height);
    if (!query.exec()){
        qDebug() << "addImage error:"
                 << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}
// Packs
vector<Pack> SqliteStorage::getAllPacks(void)
{
    vector<Pack> pack;

    QSqlQuery query("SELECT * FROM packs");
    while (query.next())
    {
       int id =  query.value("id").toInt();
       string name = query.value("name").toString().toStdString();
       string genre = query.value("genre").toString().toStdString();
       int quantity =  query.value("quantity").toInt();

       Pack p;
       p.id = id;
       p.Name = name;
       p.Genre = genre;
       p.Quantity = quantity;

       pack.push_back(p);
    }

    return pack;
}
optional<Pack> SqliteStorage::getPackById(int pack_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM packs WHERE id = :id");
    query.bindValue(":id", pack_id);
    if (!query.exec()) {
       qDebug() << "get pack error:" << query.lastError();
       return nullopt;
    }

    if (query.next()) {
        int id =  query.value("id").toInt();
        string name = query.value("name").toString().toStdString();
        string genre = query.value("genre").toString().toStdString();
        int quantity =  query.value("quantity").toInt();

        Pack p;
        p.id = id;
        p.Name = name;
        p.Genre = genre;
        p.Quantity = quantity;

        return p;
    } else {
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatePack(const Pack &pack)
{
    QSqlQuery query;
    query.prepare("UPDATE packs SET name = :name, genre = :genre, quantity = :quantity WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(pack.Name));
    query.bindValue(":genre", QString::fromStdString(pack.Genre));
    query.bindValue(":quantity", pack.Quantity);
    query.bindValue(":id", pack.id);
    if (!query.exec()){
        qDebug() << "updatePack error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }

    return true;
}
bool SqliteStorage::removePack(int pack_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM packs WHERE id = :id");
    query.bindValue(":id", pack_id);
    if (!query.exec()){
        qDebug() << "deletePack error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertPack(const Pack &pack)
{
    QSqlQuery query;
    query.prepare("INSERT INTO packs (name, genre, quantity) VALUES (:name, :genre, :quantity)");
    query.bindValue(":name", QString::fromStdString(pack.Name));
    query.bindValue(":genre", QString::fromStdString(pack.Genre));
    query.bindValue(":quantity", pack.Quantity);
    if (!query.exec()){
        qDebug() << "addPack error:"
                 << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();

    return 0;
}


vector<Image> SqliteStorage::getAllUserImages(int user_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM images "
                  "WHERE user_id = :user_id;");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    vector<Image> images;
    while (query.next())
    {
        Image i;
        i.id = query.value("id").toInt();
        i.user_id = user_id;
        i.Name = query.value("name").toString().toStdString();
        i.Data = query.value("data").toString().toStdString();
        i.height = query.value("height").toInt();
        i.width = query.value("width").toInt();
        images.push_back(i);
    }
    return images;
}

vector<Image> SqliteStorage::getAllFiltratedUserImages(int user_id, Filter filter, int current_page, int page_size)
{
    vector<Image> I ;

    QString prepare = "SELECT * FROM images WHERE user_id = :user_id AND height > :minHeight AND width > :minWidth AND height < :maxHeight AND width < :maxWidth ";

    if (filter.nameASC)
    {
        prepare += " ORDER BY name ASC";
    }else if (filter.nameDESC)
    {
        prepare += " ORDER BY name DESC";
    }else if (filter.sizeASC)
    {
        prepare += " ORDER BY height + width ASC";
    }else if(filter.sizeDESC)
    {
        prepare += " ORDER BY height + width DESC";

    }
        prepare += " LIMIT :limit OFFSET :offset";

    QSqlQuery query;
    query.prepare(prepare);
    query.bindValue(":minHeight", filter.greater_than_heigts);
    query.bindValue(":minWidth", filter.greater_than_width);
    query.bindValue(":maxHeight", filter.less_than_heigth);
    query.bindValue(":maxWidth", filter.less_than_width);
    query.bindValue(":limit", page_size);
    query.bindValue(":offset", page_size * current_page);
    query.bindValue(":user_id", user_id);

    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    while(query.next())
    {
        int image_id = query.value("id").toInt();
        Image i = *getImageById(image_id);
        I.push_back(i);
    }

    return I;
}
// users

optional<User> SqliteStorage::getUserById(int user_id)
{

    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE id = :id");
    query.bindValue(":id", user_id);
    if (!query.exec()) {
       qDebug() << "get pack error:" << query.lastError();
       return nullopt;
    }

    if (query.next()) {

        User u;
        u.id = query.value("id").toInt();
        u.username = query.value("username").toString();
        u.password_hash = query.value("password_hash").toString();
        u.avatar = query.value("avatar").value<QByteArray>();

        return u;
    }else {
        return nullopt;}

    return nullopt;

}

int SqliteStorage::insertUser(const QString &username, const QString &password)
{
    QSqlQuery query;
    query.prepare("INSERT INTO users (username, password_hash) "
                  "VALUES (:username, :password_hash)");
    query.bindValue(":username", username);
    query.bindValue(":password_hash", password);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    return query.lastInsertId().toInt();
}

void SqliteStorage::updateUser(int userId, const QString &new_username, const QString &new_password, QByteArray &new_avatar)
{
    QString isPassword = "";
    if(new_password  != "password") isPassword += "password_hash = :password_hash,";

    QSqlQuery query;
    query.prepare("UPDATE users SET username = :username, " + isPassword + " avatar = :avatar WHERE id = :id");
    query.bindValue(":id", userId);
    query.bindValue(":avatar", new_avatar);
    query.bindValue(":username", new_username);
    if(new_password  != "password")
    {
        query.bindValue(":password_hash", hashPassword( new_password ));
    }

    if (!query.exec()){
        qDebug() << "updateUser error:" << query.lastError();
        return;
    }

}
// links
vector<Pack> SqliteStorage::getAllImagePacks(int image_id)
{
    vector<Pack> P;

    QSqlQuery links_query;
    links_query.prepare("SELECT * FROM links WHERE image_id = :image_id;");
    links_query.bindValue(":image_id", image_id);
    if (!links_query.exec())
    {
        QSqlError error = links_query.lastError();
        throw error;
    }
    while (links_query.next())
    {
        int pack_id = links_query.value("pack_id").toInt();

        P.push_back(*getPackById(pack_id));
    }

    return P;
}
bool SqliteStorage::insertImagePacks(int image_id, int pack_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (image_id, pack_id) VALUES (:image_id, :pack_id)");
    query.bindValue(":image_id", image_id);
    query.bindValue(":pack_id", pack_id);
    if (!query.exec()){
        qDebug() << "addLink error:"
                 << query.lastError();
        return false;
    }
    return true;

}
bool SqliteStorage::removeImagePacks(int image_id, int pack_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE image_id = :image_id AND pack_id = :pack_id");
    query.bindValue(":pack_id", pack_id);
    query.bindValue(":image_id", image_id);
    if (!query.exec()){
        qDebug() << "deleteLink error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

QVariant SqliteStorage::authUser(const QString &username, const QString &password)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash;");
    query.bindValue(":username", username);
    query.bindValue(":password_hash", hashPassword(password) );
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }

    if (query.next())
    {
        User user;
        user.id = query.value("id").toInt();
        user.username = query.value("username").toString();
        user.password_hash = query.value("password_hash").toString();
        user.avatar = query.value("avatar").value<QByteArray>();

        return QVariant::fromValue(user);
    }
    return QVariant::fromValue(nullptr);
}

bool SqliteStorage::getUserByUsername(const QString &username)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username");
    query.bindValue(":username", username);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }

    if (query.next())
    {
        return true;
    }
    return false;
}


QString hashPassword(QString const & pass) {
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}
