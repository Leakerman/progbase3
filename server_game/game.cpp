#include "game.h"


#include <QGraphicsRectItem>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QTimer>
#include <QImage>

//QGraphicsView * view;
QMediaPlayer * music;
//int score;
extern gameSettings * settings;

Game::Game(gameSettings settings, QWidget *parent)
{
    this->parent = parent;
    //creating scene
    scene = new QGraphicsScene();

    // creating an item
    player = new Player();

    //put item on scene
    scene->addItem(player);

    //add progressBar

    bar = new QProgressBar();
    bar->setMaximum(200);
    bar->setMinimum(0);
    bar->setOrientation(Qt::Orientation::Vertical);

    //set progressBar pos
    bar->move(800 - 30,0);
    bar->resize(30,600);
    bar->setValue(player->score);

    scene->addWidget(bar);

    //add bg
    QImage image(":/data/images/bg_space.jpg");
    scene->setBackgroundBrush(QBrush(image.scaled(QSize(800,600))));


    //focus on item
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();

    //add a view
    this->view = new QGraphicsView(scene);

    this->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);


    this->view->setFixedSize(800,600);
    scene->setSceneRect(0,0,800,600);

    player->setPos(this->view->width()/2-50,this->view->height()-100);


    //spawn enemies
    QTimer * timer = new QTimer();
    QObject::connect(timer,SIGNAL(timeout()),player,SLOT(spawn()));
    timer->start(1000 - 10*player->score);


    //play bg music in a loop
    QString music_path = "qrc:/data/sounds/";
    if(settings.soundtrack == "my_hertz_brennt") music_path += "my_hertz_brennt.mp3";
    if(settings.soundtrack == "just_so") music_path += "just_so.mp3";
    if(settings.soundtrack == "holdfast") music_path += "holdfast.mp3";

    QMediaPlaylist * playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl(music_path));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);

    music  = new QMediaPlayer();
    music->setPlaylist(playlist);
    music->play();

    this->view->setWindowTitle("game");
    this->view->show();

}

