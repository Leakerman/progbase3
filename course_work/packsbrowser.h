#ifndef PACKSBROWSER_H
#define PACKSBROWSER_H

#include "pack.h"
#include "../myLibrary/mylibrary.h"
#include <QDialog>

namespace Ui {
class packsBrowser;
}

class packsBrowser : public QDialog
{
    Q_OBJECT

public:
    explicit packsBrowser(int image_id,Storage * storage,QWidget *parent = 0);
    ~packsBrowser();
    Storage * storage;
    int image_id = 0;

private slots:

    void closeEvent(QCloseEvent *event);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::packsBrowser *ui;
};

#endif // PACKSBROWSER_H
