#ifndef GAME_H
#define GAME_H


#include <QGraphicsScene>
#include <QGraphicsView>
#include <QProgressBar>
#include <QWidget>
#include "player.h"
#include "bullet.h"
#include "gamesettings.h"

class Game: public QGraphicsView
{
public:
    Game(gameSettings settings,QWidget * parent  = nullptr);

    QGraphicsView * view;
    QWidget * parent  = nullptr;
    QGraphicsScene * scene;
    QProgressBar * bar;
    Player * player;
    bool play = true;

};

#endif // GAME_H


