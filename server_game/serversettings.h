#ifndef SERVERSETTINGS_H
#define SERVERSETTINGS_H

#include "gamesettings.h"

#include <QDialog>

namespace Ui {
class serversettings;
}

class serversettings : public QDialog
{
    Q_OBJECT

public:
    explicit serversettings(QWidget *parent = 0);

    ~serversettings();

private slots:
    void on_pushButton_clicked();

private:
    Ui::serversettings *ui;
};

#endif // SERVERSETTINGS_H
