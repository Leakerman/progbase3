#ifndef CREATINGIMAGEWIDGET_H
#define CREATINGIMAGEWIDGET_H

#include <QDialog>
#include "mainwindow.h"
#include "imagebtn.h"
#include "image.h"

namespace Ui {
class creatingimagewidget;
}

class creatingimagewidget : public QDialog
{
    Q_OBJECT

public:
    explicit creatingimagewidget(string * data, string currImage ,QWidget *parent = 0, Storage *storage = nullptr, int bttnsize = 23, int width = 3, int heigth = 3);
    ~creatingimagewidget();

    int rows;
    int columns;
    std::vector<ImageButton *>  imageButtons;

    string * data;
    Storage * storage_;

private slots:
   void closeEvent(QCloseEvent *event);

private:
    Ui::creatingimagewidget *ui;
};

#endif // CREATINGIMAGEWIDGET_H
