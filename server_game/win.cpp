#include "win.h"
#include "ui_win.h"
#include <QLabel>
#include <QMovie>
#include <QDebug>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QGraphicsView>
#include "game.h"
#include "menu.h"

//extern QGraphicsView * view;
extern QMediaPlayer * music;
extern Game * game;

win::win(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::win)
{
    ui->setupUi(this);
    QMovie *movie = new QMovie(":/data/images/win.gif");
    if (movie->isValid() == false) {
        qDebug() << "Error";
        abort();
    }
    ui->label_2->setMovie(movie);
    movie->setScaledSize(QSize(this->width(), this->height()));
    movie->start();
    QMediaPlaylist * playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl("qrc:/data/sounds/win.wav"));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);

    QMediaPlayer * local_music  = new QMediaPlayer();
    local_music->setPlaylist(playlist);
    local_music->play();
    game->view->close();
    music->stop();
    this->window()->setWindowTitle("YOU WON!");
}

win::~win()
{
    delete ui;
}

void win::closeEvent(QCloseEvent *event)
{
    this->hide();
    menu m;
    m.exec();
}

void win::on_pushButton_clicked()
{
    this->hide();
    menu m;
    m.exec();
}
