#include "filterwidget.h"
#include "ui_filterwidget.h"

filterwidget::filterwidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::filter)
{
    ui->setupUi(this);
    this->setWindowTitle("Filter menu");

}

filterwidget::~filterwidget()
{
    delete ui;
}

void filterwidget::closeEvent(QCloseEvent *event)
{
    this->parentWidget()->show();
}

void filterwidget::on_pushButton_clicked()
{
    filter.greater_than_heigts = ui->greater_than_height->value();
    filter.greater_than_width = ui->greater_than_width->value();
    filter.less_than_heigth = ui->less_than_heigth->value();
    filter.less_than_width = ui->less_than_width->value();

    this->close();
    this->parentWidget()->show();
}

void filterwidget::on_nameASC_clicked()
{
    ui->sizeASC->setEnabled(false);
    ui->sizeDESC->setEnabled(false);
    ui->nameDESC->setEnabled(false);

    filter.sizeASC = false;
    filter.sizeDESC = false;
    filter.nameASC = true;
    filter.nameDESC = false;
}

void filterwidget::on_nameDESC_clicked()
{
    ui->sizeASC->setEnabled(false);
    ui->sizeDESC->setEnabled(false);
    ui->nameASC->setEnabled(false);

    filter.sizeASC = false;
    filter.sizeDESC = false;
    filter.nameASC = false;
    filter.nameDESC = true;
}

void filterwidget::on_sizeASC_clicked()
{
    ui->sizeDESC->setEnabled(false);
    ui->nameASC->setEnabled(false);
    ui->nameDESC->setEnabled(false);

    filter.sizeASC = true;
    filter.sizeDESC = false;
    filter.nameASC = false;
    filter.nameDESC = false;
}

void filterwidget::on_sizeDESC_clicked()
{
    ui->sizeASC->setEnabled(false);
    ui->nameASC->setEnabled(false);
    ui->nameDESC->setEnabled(false);

    filter.sizeASC = false;
    filter.sizeDESC = true;
    filter.nameASC = false;
    filter.nameDESC = false;
}
