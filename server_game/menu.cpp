#include "menu.h"
#include "ui_menu.h"
#include "game.h"
#include "serversettings.h"
#include <QDebug>
#include <QString>
#include <QMessageBox>

Game * game = nullptr;
extern gameSettings * settings;

menu::menu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menu)
{
    ui->setupUi(this);

    this->setWindowTitle("main menu");

    if (game != nullptr)
    {

        this->IHaveTimeForGames = false;
    }

    refresh();
}

menu::~menu()
{
    delete ui;
}

void menu::on_play_clicked()
{
    if(IHaveTimeForGames == false)
    {
        QMessageBox::warning(this,"STOP","You have no time for games");
    }else
    {

        game = new Game(*settings,this);
        this->close();
    }



}

void menu::on_settings_clicked()
{
    serversettings w;
    w.exec();
    refresh();

}

void menu::on_close_server_clicked()
{
    this->close();
}

void menu::refresh()
{
    QString difficulty ;
    if(settings->difficulty == 1)
    {
        difficulty = "easy";
    }
    if(settings->difficulty == 2)
    {
        difficulty = "medium";
    }
    if(settings->difficulty == 3)
    {
        difficulty = "hard";
    }

    ui->difficulty_2->setText(  difficulty );
    ui->player_2->setText( settings->character );
    ui->enemies_2->setText( settings->enemies );
    ui->soundtrack_2->setText( settings->soundtrack );
}

void menu::closeEvent(QCloseEvent *event)
{
    if(!IHaveTimeForGames)
    {
    delete game->player;
    delete game->view;
    delete game->bar;
    exit(EXIT_SUCCESS);
    }
}
