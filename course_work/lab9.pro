#-------------------------------------------------
#
# Project created by QtCreator 2019-05-21T18:18:36
#
#-------------------------------------------------

QT       += core gui xml sql
QT += network
QT += multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab9
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    additemwidget.cpp \
    creatingimagewidget.cpp \
    main.cpp \
    mainwindow.cpp \
    updateitemwidget.cpp \
#    sqlite_storage.cpp \
    authentification.cpp \
    registration.cpp \
    packsbrowser.cpp \
    userediting.cpp \
    filterwidget.cpp \
    serversettings.cpp

HEADERS += \
    additemwidget.h \
    creatingimagewidget.h \
    image.h \
    imagebtn.h \
    mainwindow.h \
    optional.h \
    pack.h \
#    storage.h \
    updateitemwidget.h \
#    sqlite_storage.h \
    user.h \
    authentification.h \
    registration.h \
    packsbrowser.h \
    userediting.h \
    filtersettings.h \
    filterwidget.h \
    serversettings.h \
    gamesettings.h

FORMS += \
    additemwidget.ui \
    creatingimagewidget.ui \
    mainwindow.ui \
    updateitemwidget.ui \
    authentification.ui \
    registration.ui \
    packsbrowser.ui \
    userediting.ui \
    filterwidget.ui \
    serversettings.ui

SUBDIRS += \
    lab9.pro

DISTFILES += \
    lab9.pro.user \
    sources/default-user.jpg \
    sources/milos.gif

RESOURCES += \
    res.qrc



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../progbaese_3_build/build_my_library/release/ -lmyLibrary
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../progbaese_3_build/build_my_library/debug/ -lmyLibrary
else:unix: LIBS += -L$$PWD/../../progbaese_3_build/build_my_library/ -lmyLibrary

INCLUDEPATH += $$PWD/../../progbaese_3_build/build_my_library
DEPENDPATH += $$PWD/../../progbaese_3_build/build_my_library

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../progbaese_3_build/build_my_library/release/libmyLibrary.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../progbaese_3_build/build_my_library/debug/libmyLibrary.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../progbaese_3_build/build_my_library/release/myLibrary.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../progbaese_3_build/build_my_library/debug/myLibrary.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../progbaese_3_build/build_my_library/libmyLibrary.a
