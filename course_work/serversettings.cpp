#include "serversettings.h"
#include "ui_serversettings.h"

#include <QDomDocument>
#include <QDebug>
#include <QTcpSocket>
#include <QMessageBox>

serversettings::serversettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::serversettings)
{
    ui->setupUi(this);
    this->setWindowTitle("Server settings");
}

serversettings::~serversettings()
{
    delete ui;
}

static bool sendXMLToServer(QString msg,QWidget *parent = 0);

void serversettings::on_pushButton_clicked()
{
    if(ui->difficulty_easy->isChecked())
    {
        this->settings.difficulty = 1;
    }
    if(ui->difficulty_medium->isChecked())
    {
        this->settings.difficulty = 2;
    }
    if(ui->difficulty_hard->isChecked())
    {
        this->settings.difficulty = 3;
    }


    if(ui->soundtrack_hertz->isChecked())
    {
        this->settings.soundtrack = "my_hertz_brennt";
    }
    if(ui->soundtrack_holdfast->isChecked())
    {
        this->settings.soundtrack = "holdfast";
    }
    if(ui->soundtrack_just->isChecked())
    {
        this->settings.soundtrack = "just_so";
    }



    if(ui->player_rhadyniak->isChecked())
    {
        this->settings.character = "rhadyniak";
    }
    if(ui->player_vleheza->isChecked())
    {
        this->settings.character = "vleheza";
    }
    if(ui->player_vsymoniuk->isChecked())
    {
        this->settings.character = "vsymoniuk";
    }


    if(ui->enemies_students->isChecked())
    {
        this->settings.enemies = "students";
    }
    if(ui->enemies_teachers->isChecked())
    {
        this->settings.enemies = "teachers";
    }

    QDomDocument root;
    QDomElement settings = root.createElement("settings");

    settings.setAttribute("soundtrack",this->settings.soundtrack);
    settings.setAttribute("difficulty",QString::number(this->settings.difficulty));
    settings.setAttribute("character",this->settings.character);
    settings.setAttribute("enemies",this->settings.enemies);

    root.appendChild(settings);


    QString server_settings = root.toString();
        qDebug().noquote() << server_settings;
        sendXMLToServer(server_settings,this);

        this->close();
        this->parentWidget()->close();
        this->parentWidget()->parentWidget()->show();

}

void serversettings::closeEvent(QCloseEvent *event)
{
 this->parentWidget()->show();
}



static bool sendXMLToServer(QString msg, QWidget *parent) {
    QTcpSocket client_socket;
   const int server_port = 3333;
   client_socket.connectToHost("127.0.0.1", server_port);
   if (!client_socket.waitForConnected(1000))
   {
        QMessageBox::warning(parent,"ERROR","can't connect to the server",QMessageBox::Ok);
//        return false;
   }
   qDebug() << "Client connected to server at port " << server_port;

   QByteArray request_data = msg.toUtf8();
  client_socket.write(request_data);
  if (!client_socket.waitForBytesWritten(1000))
  {
      qDebug() << "sending timeout (1000)";
      return false;
  }
  else
  {
      qDebug() << "data sent to server:>>>";
      qDebug() << msg;
      qDebug() << "<<<";
      return true;
  }
}




