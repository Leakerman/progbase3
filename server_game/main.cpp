#include <QApplication>
#include "game.h"
#include "mainwindow.h"
#include <QTimer>

#include <QCoreApplication>
#include <QTcpServer>
#include <QTcpSocket>

#include "mytcpserver.h"
#include "gamesettings.h"





int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    mainwindow * window = new mainwindow();

    QTcpServer tcp_server;  // standard class with signals
    MyTcpServer my_server;  // custom class with slots
    QObject::connect(&tcp_server, &QTcpServer::newConnection, &my_server, &MyTcpServer::onNewConnection);
    QObject::connect(&my_server, &MyTcpServer::myEmitter, window, &mainwindow::onConnected);
    const int port = 3333;
    if (tcp_server.listen(QHostAddress::Any, port))
    {
        qDebug() << "started at port " << port;
    }
    else
    {
        qDebug() << "can't start at port " << port;
    }


    window->exec();



    return a.exec();
}



void onServerError(QAbstractSocket::SocketError socketError)
{
   qDebug() << "tcp server error: " << socketError;
}

