
#include "enemy.h"
#include "player.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include <cstdlib>
#include "game.h"
#include "lose.h"

extern Game * game;
extern gameSettings * settings;
//extern QGraphicsgame->view * view;
//extern int score;

Enemy::Enemy()
{
    //drew the rect
    srand(time(0));
    int randEnemy = rand() % 6 + 1;
    int randEnemyPos = rand() % 700 + 1;

    QString enemy_type;
    int enemy_size;
    if(settings->enemies == "students")
    {
        enemy_type = "students";
        enemy_size = 70;
    }
    if(settings->enemies == "teachers")
    {
        enemy_type = "teachers";
        enemy_size = 100;
    }

    setPos(randEnemyPos,0);


    setPixmap(QPixmap(":/data/images/enemies_" + enemy_type + "/enemy"+ QString::number(randEnemy) +".png").scaled(QSize(enemy_size,enemy_size)));


    //connect
    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(300);
}


void Enemy::move()
{

    static bool lose_flag = false;
//    qDebug() << "Speed" << 15 + game->player->score/10;

    setPos(x(),y() + settings->difficulty * (20 + (float)game->bar->value()/10));
    if(pos().y() + 100 > 600)
    {
        game->player->score -=30;

        game->bar->setValue(game->player->score);
        if (this != nullptr && !game->window()->isHidden())
            scene()->removeItem(this);
        delete this;
        if (game->player->score < 0 && !lose_flag && game->player->win == false) {
            lose_flag = true;
            game->play = false;
            game->view->close();
            game->player->score = 10;
//            delete game;
            lose dialog;
            dialog.exec();

            game->close();
            delete game;

        }
    }

}
