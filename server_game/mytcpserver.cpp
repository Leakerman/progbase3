
#include "mytcpserver.h"
#include "game.h"
#include "gamesettings.h"
#include <QDomDocument>
#include "mainwindow.h"

gameSettings * settings;

gameSettings * parseSettingsXML(QString xml) {
    gameSettings * gs = new gameSettings();

     QDomDocument doc;
     doc.setContent(xml);
     QDomElement element =  doc.documentElement();
     gs->character = element.attribute("character");
     gs->difficulty = element.attribute("difficulty").toInt();
     gs->enemies = element.attribute("enemies");
     gs->soundtrack = element.attribute("soundtrack");

     return gs;
}

void MyTcpServer::onNewConnection()
{
   QObject * sender = this->sender();  // to get sender object pointer
   QTcpServer * server = static_cast<QTcpServer *>(sender);  // downcast
   //
   qDebug() << "got client new pending connection";
   QTcpSocket * new_client = server->nextPendingConnection();
   // connect client socket signals
   connect(new_client, &QTcpSocket::readyRead, this, &MyTcpServer::onClientReadyRead);
   connect(new_client, &QTcpSocket::bytesWritten, this, &MyTcpServer::onClientDataSent);
   connect(new_client, &QTcpSocket::disconnected, this, &MyTcpServer::onClientDisconnected);
   emit this->myEmitter();

}

void MyTcpServer::onClientReadyRead()
{
   QObject * sender = this->sender();  // to get sender object pointer
   QTcpSocket * client = static_cast<QTcpSocket *>(sender);  // downcast
   //
   QByteArray received_data = client->readAll();
   QString received_string = QString::fromUtf8(received_data);



   qDebug() << "data received from client:>>>";
   qDebug() << received_string;
   qDebug() << "<<<";
   settings = parseSettingsXML(received_string);


}

void MyTcpServer::onClientDataSent()
{
   qDebug() << "data sent to client.";
}

void MyTcpServer::onClientDisconnected()
{
   QObject * sender = this->sender();  // to get sender object pointer
   QTcpSocket * client = static_cast<QTcpSocket *>(sender);  // downcast
   //
   qDebug() << "client disconnected";
   client->deleteLater();  // use this instead of delete
}
