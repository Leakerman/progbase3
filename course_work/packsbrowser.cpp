#include "packsbrowser.h"
#include "ui_packsbrowser.h"

#include <QMessageBox>
#include <QtSql>
#include "../myLibrary/mylibrary.h"

packsBrowser::packsBrowser(int image_id,Storage * storage,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::packsBrowser)
{
    ui->setupUi(this);
    this->storage = storage;
    this->image_id = image_id;


    std::vector<Pack >  entity = storage->getAllPacks();
    foreach(auto pack , entity)
    {
        std::vector<Pack> check = this->storage->getAllImagePacks(this->image_id);
        bool add = true;

        for(auto i : check )
        {
            if(i.id == pack.id)
            {
                add = false;
            }
        }

        if(add)
        {
        QVariant qvar;
        qvar.setValue(pack);
        QListWidgetItem * item = new QListWidgetItem( );
        item->setText( QString::fromStdString(pack.Name) );
        item->setData(Qt::UserRole, qvar);
        ui->listWidget->addItem(item);
        }
    }

}

packsBrowser::~packsBrowser()
{
    delete ui;
}

void packsBrowser::closeEvent(QCloseEvent *event)
{
    this->parentWidget()->show();
}

void packsBrowser::on_pushButton_clicked()
{
    QList<QListWidgetItem *> list = ui->listWidget->selectedItems();
    if (list.count() > 0)
    {
      QListWidgetItem * item = list.at(0);
      int itemRow = ui->listWidget->row(item);

      QVariant qvar;
      qvar = item->data(Qt::UserRole);
      Pack pack = qvar.value<Pack>();

      this->storage->insertImagePacks(this->image_id,pack.id);
    }
}

void packsBrowser::on_pushButton_2_clicked()
{
    this->close();
    this->parentWidget()->show();
}
