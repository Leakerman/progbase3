#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "../myLibrary/mylibrary.h"
#include "user.h"
#include "filtersettings.h"
#include "gamesettings.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    User user;
    gameSettings settings;
    Filter  filter{false,false,false,false,2,2,41,41};

    int pageSize = 7;
    int currentPage = 0;
    int maxPage = 0;

    void refresh();

    void authentificate();

private slots:
    void on_actionOpen_storage_triggered();

    void on_addButton_clicked();

    void on_updateButton_clicked();

    void on_writersListWidget_itemClicked(QListWidgetItem *item);

    void on_removeButton_clicked();

    void on_actionExit_triggered();

    void on_actionSign_out_triggered();

    void on_actionEdit_user_triggered();




    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_actionExport_triggered();

    void on_actionimport_triggered();

    void on_actionFilter_triggered();

private:
    Storage * storage_;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
