#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>

class MyTcpServer : public QObject
{
   Q_OBJECT
public:
   explicit MyTcpServer(QObject * parent = nullptr): QObject(parent) {}

public slots:
   void onNewConnection();
   void onClientReadyRead();
   void onClientDataSent();
   void onClientDisconnected();
signals:
   void myEmitter();


};


#endif // MYTCPSERVER_H
