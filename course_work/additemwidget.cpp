#include "additemwidget.h"
#include "ui_additemwidget.h"
#include <QDebug>
#include "creatingimagewidget.h"

additemwidget::additemwidget(int current_user_id, QWidget *parent, Storage *storage) :
    QDialog(parent),
    ui(new Ui::additemwidget)
{
    this->current_user_id = current_user_id;
    this->storage_ = storage;
    ui->setupUi(this);
}

additemwidget::~additemwidget()
{
    delete ui;
}

void additemwidget::closeEvent(QCloseEvent *event)
{
    this->parentWidget()->show();
}

void additemwidget::on_pushButton_clicked()
{
        creatingimagewidget  w(&data ,"",this , this->storage_ ,ui->btnsize->value() , ui->width->value() , ui->heigth->value() );
        w.setWindowTitle("Creating new image");
        w.exec();

        ui->btnsize->close();
        ui->heigth->close();
        ui->width->close();

        ui->pushButton->setEnabled(false);
}

void additemwidget::on_pushButton_2_clicked()
{
   Image image;
   image.user_id = this->current_user_id;
   qDebug() << image.user_id;
   image.Name  = ui->lineEdit->text().toStdString();
   image.width = ui->width->value();
   image.height = ui->heigth->value();
   image.Data = data;
   this->storage_->insertImage(image);
   this->close();
   this->parentWidget()->show();

}
