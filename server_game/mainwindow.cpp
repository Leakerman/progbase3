#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "menu.h"




mainwindow::mainwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::mainwindow)
{
    ui->setupUi(this);
    ui->pushButton->setEnabled(false);
    this->setWindowTitle("Waiting room");
}

mainwindow::~mainwindow()
{
    delete ui;

}

void mainwindow::on_pushButton_clicked()
{
    this->close();
    menu m;
    m.exec();
}

void mainwindow::onConnected()
{
    this->ui->label->setText("         connected");
    this->ui->pushButton->setEnabled(true);
}
