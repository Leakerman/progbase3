#ifndef AUTHENTIFICATION_H
#define AUTHENTIFICATION_H

#include <QDialog>
#include "../myLibrary/mylibrary.h"

namespace Ui {
class authentification;
}

class authentification : public QDialog
{
    Q_OBJECT

public:
    explicit authentification(Storage * storage,QWidget *parent = 0);
    ~authentification();

    Storage * storage;
    User user;
    int currentUserId = 0;
    bool userExist = false;

private slots:

    void closeEvent(QCloseEvent *event);

    void on_log_in_clicked();

    void on_sign_in_clicked();

private:
    Ui::authentification *ui;
};

#endif // AUTHENTIFICATION_H
