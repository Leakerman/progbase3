#include "authentification.h"
#include "ui_authentification.h"
#include <QtSql>
#include "image.h"
#include "registration.h"
#include <QMessageBox>
#include "../myLibrary/mylibrary.h"
#include "user.h"

QVariant authUser(QString const & username, QString const & password)
{
   QSqlQuery query;
   query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash;");
   query.bindValue(":username", username);
   query.bindValue(":password_hash", hashPassword(password) );
   if (!query.exec())
   {
       QSqlError error = query.lastError();
       throw error;
   }

   if (query.next())
   {
       User user;
       user.id = query.value("id").toInt();
       user.username = query.value("username").toString();
       user.password_hash = query.value("password_hash").toString();
       user.avatar = query.value("avatar").value<QByteArray>();

       return QVariant::fromValue(user);
   }
   return QVariant::fromValue(nullptr);
}

authentification::authentification(Storage *storage, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::authentification)
{
    this->storage = storage;
    this->userExist = false;
    ui->setupUi(this);
}

authentification::~authentification()
{
    delete ui;
}

void authentification::closeEvent(QCloseEvent *event)
{
    this->parentWidget()->show();
}


void authentification::on_log_in_clicked()
{
    QString username = ui->username->text();
    QString password_hash = ui->password->text();

    QVariant user_variant = this->storage->authUser(username,password_hash);
    if (user_variant.isNull())
    {
       QMessageBox::warning(this,"authentification error","wrong password or username");
    }
    else
    {
       this->user = user_variant.value<User>();

       userExist = true;
       this->currentUserId = user.id;
       this->close();
       this->parentWidget()->show();
    }
}

void authentification::on_sign_in_clicked()
{
    this->hide();
    registration w(this->storage,this);
    w.exec();

    userExist = w.isValid;
    this->currentUserId = w.newUserId;
    this->close();
    this->parentWidget()->show();
}
