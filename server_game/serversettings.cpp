#include "serversettings.h"
#include "ui_serversettings.h"

#include <QDomDocument>
#include <QDebug>
#include <QTcpSocket>

extern gameSettings * settings;

serversettings::serversettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::serversettings)
{
    ui->setupUi(this);

    this->setWindowTitle("settings");

   if( settings->difficulty == 1 )
   {
       ui->difficulty_easy->setChecked(true);
   }
   if( settings->difficulty == 2 )
   {
       ui->difficulty_medium->setChecked(true);
   }
   if( settings->difficulty == 3 )
   {
       ui->difficulty_hard->setChecked(true);
   }

   if( settings->character == "rhadyniak" )
   {
       ui->player_rhadyniak->setChecked(true);
   }
   if( settings->character == "vleheza" )
   {
       ui->player_vleheza ->setChecked(true);
   }
   if( settings->character == "vsymoniuk" )
   {
       ui->player_vsymoniuk->setChecked(true);
   }

   if(settings->enemies == "students" )
   {
       ui->enemies_students->setChecked(true);
   }
   if(settings->enemies == "teachers" )
   {
       ui->enemies_teachers->setChecked(true);
   }

   if(settings->soundtrack == "my_hertz_brennt" )
   {
       ui->soundtrack_hertz->setChecked(true);
   }
   if(settings->soundtrack == "holdfast" )
   {
       ui->soundtrack_holdfast->setChecked(true);
   }
   if(settings->soundtrack == "just_so" )
   {
       ui->soundtrack_just->setChecked(true);
   }

}

serversettings::~serversettings()
{
    delete ui;
}

static bool sendXMLToServer(QString msg);

void serversettings::on_pushButton_clicked()
{
    if(ui->difficulty_easy->isChecked())
    {
        settings->difficulty = 1;
    }
    if(ui->difficulty_medium->isChecked())
    {
        settings->difficulty = 2;
    }
    if(ui->difficulty_hard->isChecked())
    {
        settings->difficulty = 3;
    }


    if(ui->soundtrack_hertz->isChecked())
    {
        settings->soundtrack = "my_hertz_brennt";
    }
    if(ui->soundtrack_holdfast->isChecked())
    {
        settings->soundtrack = "holdfast";
    }
    if(ui->soundtrack_just->isChecked())
    {
        settings->soundtrack = "just_so";
    }



    if(ui->player_rhadyniak->isChecked())
    {
        settings->character = "rhadyniak";
    }
    if(ui->player_vleheza->isChecked())
    {
        settings->character = "vleheza";
    }
    if(ui->player_vsymoniuk->isChecked())
    {
        settings->character = "vsymoniuk";
    }


    if(ui->enemies_students->isChecked())
    {
        settings->enemies = "students";
    }
    if(ui->enemies_teachers->isChecked())
    {
        settings->enemies = "teachers";
    }



    this->close();
}

