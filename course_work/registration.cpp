#include "registration.h"
#include "ui_registration.h"
#include <QtSql>
#include <QMessageBox>
#include "../myLibrary/mylibrary.h"
#include "authentification.h"
#include <QJsonObject>

registration::registration(Storage *storage, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registration)
{
    ui->setupUi(this);
    this->storage = storage;
    this->setWindowTitle("registration");

    manager = new QNetworkAccessManager();
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(managerFinished(QNetworkReply*)));
}

registration::~registration()
{
    delete ui;
    delete manager;
}

void registration::on_confirm_btn_clicked()
{
    if(ui->enter_password->text() != ui->confirm_password->text() )
    {
        QMessageBox::warning(this,"password error","passwords do not match");
        ui->enter_password->clear();
        ui->confirm_password->clear();
        return;
    }
    if(ui->enter_password->text().isEmpty() || ui->enter_username->text().isEmpty())
    {
        QMessageBox::warning(this,"registration error","empty data");
        ui->enter_password->clear();
        ui->confirm_password->clear();
        return;
    }
    if(storage->getUserByUsername( ui->enter_username->text()))
    {
        QMessageBox::warning(this,"username error","username does already exist");
        ui->enter_username->clear();
        return;
    }

    this->newUserId = this->storage->insertUser(ui->enter_username->text(), hashPassword(ui->enter_password->text()));
    this->isValid = true;
    this->close();
    this->parentWidget()->show();
}

void registration::managerFinished(QNetworkReply *reply)
{
    if (reply->error()) {
        qDebug() << reply->errorString();
        return;
    }

    QString answer = reply->readAll();

    QJsonDocument jsonResponse = QJsonDocument::fromJson(answer.toUtf8());

    QJsonObject jsonObject = jsonResponse.object();

    randomNickname= jsonObject["nickname"].toString();

}



void registration::on_pushButton_clicked()
{
    request.setUrl(  QUrl("https://api.codetunnel.net/random-nick"));
    QString json = QString("{\"theme\": \"%1\", \"sizeLimit\": \"%2\"}").arg(ui->lineEdit->text()).arg(ui->spinBox->value());
    manager->post(request,json.toUtf8());
    ui->enter_username->setText(randomNickname);
}

