#ifndef MYLIBRARY_H
#define MYLIBRARY_H

#include "sqlite_storage.h"
#include "storage.h"
#include <QCryptographicHash>

QString hashPassword(QString const & pass);


#endif // MYLIBRARY_H
