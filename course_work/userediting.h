#ifndef USEREDITING_H
#define USEREDITING_H

#include <QDialog>
#include <QPixmap>
#include "user.h"
#include <QByteArray>
#include "../myLibrary/mylibrary.h"

namespace Ui {
class userediting;
}

class userediting : public QDialog
{
    Q_OBJECT

public:
    explicit userediting(Storage * storage,User user,QWidget *parent = 0);
    User user;
    Storage * storage;
    ~userediting();
    bool isEntered = false;

private slots:

    void closeEvent(QCloseEvent *event);

    void on_pushButton_clicked();

    void on_changeavatar_clicked();

    void on_password_cursorPositionChanged(int arg1, int arg2);

    void on_password_confirm_cursorPositionChanged(int arg1, int arg2);

private:
    Ui::userediting *ui;
};

#endif // USEREDITING_H
