#ifndef MENU_H
#define MENU_H

#include <QDialog>

namespace Ui {
class menu;
}

class menu : public QDialog
{
    Q_OBJECT

    bool IHaveTimeForGames = true;

public:
    explicit menu(QWidget *parent = 0);
    ~menu();

private slots:
    void on_play_clicked();

    void on_settings_clicked();

    void on_close_server_clicked();

    void refresh();

      void closeEvent(QCloseEvent *event);

private:
    Ui::menu *ui;
};

#endif // MENU_H
