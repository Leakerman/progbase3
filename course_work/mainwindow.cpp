#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDir>
#include <QLineEdit>
#include <QDebug>


#include "filterwidget.h"
#include "authentification.h"
#include "userediting.h"
#include "creatingimagewidget.h"
#include "additemwidget.h"
#include "updateitemwidget.h"
#include <QVariant>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QString>
#include <QDebug>
#include <QList>
#include <QDomElement>
#include "image.h"
#include "../myLibrary/mylibrary.h"



static QDomElement imageToDomElement(QDomDocument & imagesDoc, Image & iter);
static Image domElementToImage(QDomElement & element);


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "choose SQLITE storage you want to work with",  // caption
                "../../../",                // directory to start with
                "SQLITE (*.sqlite)");  // file name filter
    if(fileName.isEmpty())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this,"ERROR","undefined storage",QMessageBox::Ok);
        if(reply == QMessageBox::Ok)
        {
            exit(EXIT_SUCCESS);
        }

    }



    this->storage_ = new  SqliteStorage(fileName.toStdString());
    this->storage_->open();

    authentificate();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_storage_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "choose SQLITE storage you want to work with",  // caption
                "/home/wawah",                // directory to start with
                "SQLITE (*.sqlite)");  // file name filter
    if(fileName.isEmpty())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this,"ERROR","invalid storage",QMessageBox::Ok);
        if(reply == QMessageBox::Ok)
        {
            return;
        }

    }


    this->storage_->close();

    this->storage_ = new  SqliteStorage(fileName.toStdString());
    this->storage_->open();

    authentificate();
}

void MainWindow::on_addButton_clicked()
{
    this->hide();
    additemwidget  w(user.id,this,this->storage_);
    w.setWindowTitle("Creating new image");
    w.exec();

    refresh();
}

void MainWindow::on_updateButton_clicked()
{
    QList<QListWidgetItem *> list = ui->writersListWidget->selectedItems();
    if (list.count() > 0)
    {
        QListWidgetItem * item = list.at(0);
        int itemRow = ui->writersListWidget->row(item);

        QVariant qvar;
        qvar = item->data(Qt::UserRole);
        Image image = qvar.value<Image>();

        this->hide();
        updateitemwidget  w(this ,this->storage_ ,&image);
        QLineEdit * line = w.findChild<QLineEdit *>("Name");
        line->setText(QString::fromStdString(image.Name));
        w.setWindowTitle("Updating image");
        w.exec();
    }

    refresh();
}

void MainWindow::on_removeButton_clicked()
{

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
                this,
                "On delete",
                "Are you sure?",
                QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QList<QListWidgetItem *> list = ui->writersListWidget->selectedItems();
        if (list.count() > 0)
        {
            QListWidgetItem * item = list.at(0);
            int itemRow = ui->writersListWidget->row(item);

            QVariant qvar;
            qvar = item->data(Qt::UserRole);
            Image image = qvar.value<Image>();

            this->storage_->removeImage(image.id);
        }

        ui->itemData->clear();
        ui->itemName->clear();
        ui->itemHeigth->clear();
        ui->itemWidth->clear();
        ui->itemCategories->clear();

    } else {
        qDebug() << "Yes was *not* clicked";
    }


    refresh();

    if(currentPage >= maxPage)
    {
        currentPage --;
        refresh();
    }
}

void MainWindow::on_writersListWidget_itemClicked(QListWidgetItem *item)
{
    QList<QListWidgetItem *> list = ui->writersListWidget->selectedItems();
    if (list.count() > 0)
    {
        QListWidgetItem * selectedItem = list.at(0);
        int itemRow = ui->writersListWidget->row(selectedItem);
        item =  selectedItem;

        QVariant qvar;
        qvar = item->data(Qt::UserRole);
        Image image = qvar.value<Image>();
        ui->itemName->setText(QString::fromStdString(image.Name));
        ui->itemWidth->setText(QString::number(image.width));
        ui->itemHeigth->setText(QString::number(image.height));
        ui->itemData->setText(QString::fromStdString(image.Data));

        QString categories = "";
        vector<Pack> C = this->storage_->getAllImagePacks(image.id);
        for(auto c : C)
        {
            categories += QString::fromStdString(c.Name);
            categories += " ";
        }
        ui->itemCategories->setText(categories);
    }
}



void MainWindow::refresh()
{
    ui->writersListWidget->clear();

    std::vector<Image > entity = this->storage_->getAllFiltratedUserImages(user.id,this->filter,currentPage,pageSize);
    std::vector<Image > size = this->storage_->getAllUserImages(user.id);

qDebug() << "sos";

    for(auto image : entity)
    {
        QVariant qvar;
        qvar.setValue(image);
        QListWidgetItem * item = new QListWidgetItem();
        item->setText( QString::fromStdString(image.Name) );
        item->setData(Qt::UserRole, qvar);
        ui->writersListWidget->addItem(item);
    }

    int maxPages;


    if (size.size() % pageSize == 0)
        maxPage = size.size() / pageSize;
    else
        maxPage= size.size() / pageSize + 1;

    if(currentPage < 1) ui->pushButton_2->setEnabled(false);
    else ui->pushButton_2->setEnabled(true);

    if(currentPage >= maxPage - 1) ui->pushButton_3->setEnabled(false);
    else ui->pushButton_3->setEnabled(true);

}




void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionSign_out_triggered()
{
    authentificate();
    currentPage = 0;
}

void MainWindow::on_actionEdit_user_triggered()
{
    this->hide();
    userediting w(this->storage_,this->user,this);
    w.exec();

    this->user = w.user;
}


QDomElement imageToDomElement(QDomDocument & imagesDoc, Image & iter)
{
    QDomElement image = imagesDoc.createElement("image");


    image.setAttribute("id", iter.id);
    image.setAttribute("Name", iter.Name.c_str());
    image.setAttribute("Data", iter.Data.c_str());
    image.setAttribute("width", iter.width);
    image.setAttribute("height", iter.height);

    return image;
}

Image domElementToImage(QDomElement & element)
{
    Image image;

    image.id = element.attribute("id").toInt();
    image.Name = element.attribute("Name").toStdString();
    image.Data = element.attribute("Data").toStdString();
    image.width = element.attribute("width").toInt();
    image.height = element.attribute("height").toInt();

    return image;
}

void MainWindow::authentificate()
{
    this->hide();
    authentification w(this->storage_,this);
    w.setWindowTitle("authetification");
    w.exec();

    if(w.userExist)
    {
        currentPage = 0;

        this->user = *this->storage_->getUserById(w.currentUserId);

        ui->addButton->setEnabled(true);
        ui->removeButton->setEnabled(true);
        ui->updateButton->setEnabled(true);

        ui->actionExport->setEnabled(true);
        ui->actionimport->setEnabled(true);
        ui->actionFilter->setEnabled(true);
        ui->actionEdit_user->setEnabled(true);

        refresh();
    }else {

        ui->addButton->setEnabled(false);
        ui->removeButton->setEnabled(false);
        ui->updateButton->setEnabled(false);
        ui->actionFilter->setEnabled(false);
        ui->actionExport->setEnabled(false);
        ui->actionimport->setEnabled(false);

        ui->actionEdit_user->setEnabled(false);

        ui->writersListWidget->clear();
        ui->itemCategories->clear();
        ui->itemData->clear();
        ui->itemHeigth->clear();
        ui->itemName->clear();
        ui->itemWidth->clear();
    }

}

void MainWindow::on_pushButton_2_clicked()
{
    this->currentPage -= 1;
    refresh();
}

void MainWindow::on_pushButton_3_clicked()
{
    this->currentPage += 1;
    refresh();
}

void MainWindow::on_actionExport_triggered()
{
    QString imagesFilename = QFileDialog::getOpenFileName(
                this,              // parent
                "File to export",  // caption
                "../../../",                // directory to start with
                "XML (*.xml);;All Files (*)");  // file name filter

    QFile imagesFile(imagesFilename);

    if(!imagesFile.open(QFile::ReadOnly))
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this,
                                     "ERROR",
                                     "file was not open for writing",
                                     QMessageBox::Ok);
    }
    QTextStream imagesTS(&imagesFile);
    QString imagesData = imagesTS.readAll();
    imagesFile.close();

    QDomDocument imagesDoc;
    QString imagesXmlErrorText;
    int imagesXmlErrorColumn;
    int imagesXmlErrorLine;
    if(!imagesDoc.setContent(imagesData, &imagesXmlErrorText, &imagesXmlErrorLine, &imagesXmlErrorColumn))
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this,
                                     "ERROR",
                                     "images setContent error\n" + imagesXmlErrorText + "\n Line" + imagesXmlErrorLine,
                                     QMessageBox::Ok);
    }
    QDomElement imagesRoot =  imagesDoc.documentElement();
    for(int i = 0; i < imagesRoot.childNodes().size(); i++)
    {
        QDomNode node = imagesRoot.childNodes().at(i);
        QDomElement element =  node.toElement();
        Image image = domElementToImage(element);
        image.user_id = this->user.id;
        this->storage_->insertImage(image);
    }

    refresh();
}

void MainWindow::on_actionimport_triggered()
{
    QDomDocument imagesDoc;
    QDomElement imagesRoot = imagesDoc.createElement("images");
    for(auto iter : this->storage_->getAllUserImages(this->user.id))
    {
        QDomElement image = imageToDomElement(imagesDoc, iter);
        imagesRoot.appendChild(image);
    }
    imagesDoc.appendChild(imagesRoot);
    QString imagesData = imagesDoc.toString(4);


    QString imagesFilename = QFileDialog::getSaveFileName(
                this,              // parent
                "File to import. Enter name + .xml to save",  // caption
                "../../../",                // directory to start with
                "SQLITE (*.sqlite);;All Files (*)");  // file name filter

    QFile imagesFile(imagesFilename);
    if(!imagesFile.open(QFile::WriteOnly))
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this,"ERROR","file was not open for writing",QMessageBox::Ok);
    }
    QTextStream imagesTS(&imagesFile);

    imagesTS << imagesData;
    imagesFile.close();
}

void MainWindow::on_actionFilter_triggered()
{
    this->hide();
    filterwidget w(this);
    w.exec();


    filter.nameASC = w.filter.nameASC;
    filter.nameDESC = w.filter.nameDESC;
    filter.sizeASC = w.filter.sizeASC;
    filter.sizeDESC = w.filter.sizeDESC;
    qDebug() <<filter.sizeDESC;
    filter.greater_than_heigts = w.filter.greater_than_heigts;
    filter.greater_than_width = w.filter.greater_than_width;
    filter.less_than_heigth = w.filter.less_than_heigth;
    filter.less_than_width = w.filter.less_than_width;

    refresh();
}


