#ifndef STORAGE_H
#define STORAGE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "image.h"
#include "pack.h"
#include "user.h"
//#include "filterwidget.h"
#include "filtersettings.h"


using namespace std;

class Storage
{

  public:
    virtual ~Storage() {}

    virtual bool open() = 0;
    virtual bool close() = 0;
    // Images
    virtual vector<Image>  getAllImages(void) = 0;
    virtual vector<Image> getAllUserImages(int user_id) = 0;
    virtual vector<Image> getAllFiltratedUserImages(int user_id, Filter filter, int current_page = 1, int page_size = 7) = 0;
    virtual optional<Image> getImageById(int image_id) = 0;
    virtual bool updateImage(const Image &image) = 0;
    virtual bool removeImage(int image_id) = 0;
    virtual int insertImage(const Image &image) = 0;
    // Packs
    virtual vector<Pack> getAllPacks(void) = 0;
    virtual optional<Pack> getPackById(int pack_id) = 0;
    virtual bool updatePack(const Pack &pack) = 0;
    virtual bool removePack(int pack_id) = 0;
    virtual int insertPack(const Pack &pack) = 0;
    // users
    virtual QVariant authUser(QString const & username, QString const & password) = 0;
    virtual optional<User> getUserById(int user_id) = 0;
    virtual int insertUser(const QString &username,const QString &password) = 0;
    virtual void updateUser(int userId,const QString &new_username, const QString &new_password, QByteArray & new_avatar) = 0;
    virtual bool getUserByUsername(const QString &username) = 0;
    // links
    virtual vector<Pack> getAllImagePacks(int image_id) = 0;
    virtual bool insertImagePacks(int image_id, int pack_id) = 0;
    virtual bool removeImagePacks(int image_id, int pack_id) = 0;
};

#endif // STORAGE_H
