#include "userediting.h"
#include "ui_userediting.h"

#include <QtSql>
#include <QGraphicsPixmapItem>
#include <QMessageBox>
#include <QFileDialog>
#include "../myLibrary/mylibrary.h"

userediting::userediting(Storage *storage, User user, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userediting)
{
    ui->setupUi(this);

    this->user = user;
    this->storage = storage;
    ui->username->setText(user.username);

    this->setWindowTitle("User editing menu");

    QGraphicsPixmapItem item;
    if(this->user.avatar.isNull())
    {
        item.setPixmap(QPixmap(":/image/sources/user.jpg").scaled(QSize(150,170)));
    }
    else
    {
        QPixmap p;
        p.loadFromData(user.avatar,"PNG");
        item.setPixmap(p.scaled(QSize(150,170)));
    }
    ui->avatar->setPixmap(item.pixmap());
}

userediting::~userediting()
{
    delete ui;
}

void userediting::closeEvent(QCloseEvent *event)
{
    this->parentWidget()->show();
}

void userediting::on_pushButton_clicked()
{
    if(ui->password->text() != ui->password_confirm->text() )
    {
        QMessageBox::warning(this,"password error","passwords do not match");
        ui->password->clear();
        ui->password_confirm->clear();
        return;
    }
    if(ui->password->text().isEmpty() || ui->username->text().isEmpty() )
    {
        QMessageBox::warning(this,"ERROR","empty data");
        ui->password->clear();
        ui->password_confirm->clear();
        return;
    }
    if(ui->username->text() != user.username)
    {
        if(storage->getUserByUsername( ui->username->text() ))
        {
            QMessageBox::warning(this,"username error","username does already exist");
            ui->username->clear();
            return;
        }}

    this->storage->updateUser(this->user.id, ui->username->text() ,ui->password->text(),this->user.avatar);

    this->user.username = ui->username->text();
    this->user.password_hash = hashPassword(ui->password->text());

    this->close();
    this->parentWidget()->show();
}

void userediting::on_changeavatar_clicked()
{
    QString avatarPath = QFileDialog::getOpenFileName(
                this,              // parent
                "choose png, xpm or jpg image",  // caption
                "/home/wawah",                // directory to start with
                "Images (*.png *.xpm *.jpg)");  // file name filter
    QPixmap pixmap(avatarPath);
    QBuffer buffer(&this->user.avatar);
    buffer.open(QIODevice::WriteOnly);
    pixmap.save(&buffer, "PNG");
}

void userediting::on_password_cursorPositionChanged(int arg1, int arg2)
{
    if(!isEntered)
    {
        isEntered = true;
        ui->password->clear();
        ui->password_confirm->clear();
    }
}

void userediting::on_password_confirm_cursorPositionChanged(int arg1, int arg2)
{
    if(!isEntered)
    {
        isEntered = true;
        ui->password->clear();
        ui->password_confirm->clear();
    }
}
