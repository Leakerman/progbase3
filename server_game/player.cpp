
#include "player.h"
#include "bullet.h"
#include "enemy.h"
#include <QKeyEvent>
#include <QGraphicsScene>
#include "game.h"


extern Game * game;
extern gameSettings * settings;

Player::Player()
{

    if(settings->character == "rhadyniak") this->player = "rhadyniak";
    if(settings->character == "vsymoniuk") this->player = "vsymoniuk";
    if(settings->character == "vleheza") this->player = "vleheza";

    setPixmap(QPixmap(":/data/images/player_" + this->player + "/player.png"));
}

void Player::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left)
    {
        if(pos().x() > 0)
            setPos(x()-10,y());
        setPixmap(QPixmap(":/data/images/player_" + this->player+ "/player.png"));
    }else if(event->key() == Qt::Key_Right)
    {
        if(pos().x() + 100 < 800)
            setPos(x()+10,y());
        setPixmap(QPixmap(":/data/images/player_" + this->player+ "/player.png"));
    }else if(event->key() == Qt::Key_Space)
    {
        //create a bullet
        setPixmap(QPixmap(":/data/images/player_" + this->player+ "/player_shooting.png"));

        if(game->player->score <= 100)
        {
            Bullet * bullet = new Bullet;
            scene()->addItem(bullet);
            Bullet * bullet2 = new Bullet;
            scene()->addItem(bullet2);

            bullet->setPos(x(),y());
            bullet2->setPos(x()+100,y());

        }else if(game->player->score >= 100)
        {
            setPixmap(QPixmap(":/data/images/player_" + this->player+ "/player_shooting2.png"));
            Bullet * bullet = new Bullet;
            bullet->setPos(x()+50,y());
            scene()->addItem(bullet);
        }
    }

}

void Player::spawn()
{
    Enemy * enemy = new Enemy();
    scene()->addItem(enemy);
}

