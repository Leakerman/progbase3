#ifndef SERVERSETTINGS_H
#define SERVERSETTINGS_H

#include "gamesettings.h"

#include <QDialog>

namespace Ui {
class serversettings;
}

class serversettings : public QDialog
{
    Q_OBJECT

public:
    explicit serversettings(QWidget *parent = 0);

    gameSettings settings;

    ~serversettings();

    //     bool sendXMLToServer(QString msg);

private slots:
    void on_pushButton_clicked();

    void closeEvent(QCloseEvent *event);

private:
    Ui::serversettings *ui;
};

#endif // SERVERSETTINGS_H
