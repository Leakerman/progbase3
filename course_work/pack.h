#pragma once
#include <iostream>
#include <QMetaType>

struct Pack 
{
    int id;
    std::string Name;
    std::string Genre;
    int Quantity; 
};
Q_DECLARE_METATYPE(Pack)


