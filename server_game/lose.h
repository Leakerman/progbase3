#ifndef LOSE_H
#define LOSE_H

#include <QDialog>

namespace Ui {
class lose;
}

class lose : public QDialog
{
    Q_OBJECT

public:
    explicit lose(QWidget *parent = 0);
    ~lose();

private slots:

    void closeEvent(QCloseEvent *event);


    void on_main_menu_clicked();

private:
    Ui::lose *ui;
};

#endif // LOSE_H
