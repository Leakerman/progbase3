#ifndef ADDITEMWIDGET_H
#define ADDITEMWIDGET_H

#include <QDialog>
#include "../myLibrary/mylibrary.h"
#include "image.h"

namespace Ui {
class additemwidget;
}

class additemwidget : public QDialog
{
    Q_OBJECT

public:
    explicit additemwidget(int current_user_id,QWidget *parent = 0,Storage *storage = nullptr);
    ~additemwidget();

    Storage *  storage_;
    string data;
    int current_user_id = 0;

private slots:
    void closeEvent(QCloseEvent *event);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


private:
    Ui::additemwidget *ui;
};

#endif // ADDITEMWIDGET_H
