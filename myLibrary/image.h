#ifndef IMAGE_H
#define IMAGE_H

#include <string>
#include <QMetaType>

struct Image
{
    int id;
    std::string Name;
    std::string Data;
    int width;
    int height;
    int user_id;
};
Q_DECLARE_METATYPE(Image)

#endif // IMAGE_H
