#include "mainwindow.h"
#include <QApplication>
#include "authentification.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle("image database");
    w.show();
    return a.exec();
}
