#ifndef IMAGEBTN_H
#define IMAGEBTN_H


#include <QPushButton>


class ImageButton : public QPushButton
{
    Q_OBJECT
public:
    ImageButton(const QString & text, QWidget * parent = 0) : QPushButton(text , parent){}
    virtual ~ImageButton(){}
    bool isFilled = false;

private slots :

    void changeStatus()
    {
        if(!isFilled)
        {
            this->setPalette(Qt::black);
        }else
        {
            this->setPalette(Qt::white);
        }
        isFilled = !isFilled;
    }

};

#endif // IMAGEBTN_H
