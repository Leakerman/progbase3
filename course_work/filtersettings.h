#ifndef FILTERSETTINGS_H
#define FILTERSETTINGS_H

struct Filter
{
    bool nameASC = false;
    bool nameDESC = false;
    bool sizeASC = false;
    bool sizeDESC = false;
    int greater_than_width = 2;
    int greater_than_heigts = 2;
    int less_than_width = 41;
    int less_than_heigth = 41;
};

#endif // FILTERSETTINGS_H
