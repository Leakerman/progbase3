#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H

#include <QString>

struct gameSettings
{
    int difficulty;
    QString soundtrack;
    QString character;
    QString enemies;
};

#endif // GAMESETTINGS_H
