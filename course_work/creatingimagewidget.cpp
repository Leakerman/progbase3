#include "creatingimagewidget.h"
#include "ui_creatingimagewidget.h"
#include "imagebtn.h"
#include "mainwindow.h"

#include <QPushButton>
#include <QWidget>
#include <QDebug>
#include <QGridLayout>
#include <QObject>
#include <QString>

#include <string>

creatingimagewidget::creatingimagewidget(string *data, string currImage ,QWidget *parent, Storage * storage, int bttnsize, int width, int heigth) :
    QDialog(parent),
    ui(new Ui::creatingimagewidget)
{
    ui->setupUi(this);
    this->storage_ = storage;
    this->data = data;

    this->columns = width;
    this->rows = heigth;

    ImageButton * mapbutton[rows][columns];

    bool newImage = true;
    if(currImage.size() == 0) newImage = false;
    int k = 0;

    const int bttnsz = bttnsize;
    QGridLayout * grid = new QGridLayout(this);

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < columns ; j++)
        {
            mapbutton[i][j] = new ImageButton("");
            if(currImage[k] == '1')
            {
                mapbutton[i][j]->isFilled = true;
                mapbutton[i][j]->setPalette(Qt::black);
            }
            mapbutton[i][j]->setMaximumSize(bttnsz, bttnsz);
            mapbutton[i][j]->move(bttnsz * i, bttnsz * j);
            grid->addWidget(mapbutton[i][j], i, j);
            mapbutton[i][j]->show();

            connect(mapbutton[i][j],SIGNAL(clicked()),mapbutton[i][j],SLOT(changeStatus()));

            this->imageButtons.push_back( mapbutton[i][j]);
            k++;
        }
    }

      this->setFixedSize((columns + 2)* bttnsz ,(rows+ 2) * bttnsz);
}

creatingimagewidget::~creatingimagewidget()
{
    delete ui;
}

void creatingimagewidget::closeEvent(QCloseEvent *event)
{
    std::string imageData ;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < columns ; j++)
        {
            if(imageButtons[i * columns + j]->isFilled == true)
            {
                imageData += "1";
            }
            else
            {
                imageData += "0";
            }
            delete imageButtons[i * columns + j];
        }
    }
   this->data->append(imageData);


}

