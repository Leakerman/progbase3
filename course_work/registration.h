#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include <QtNetwork>
#include "../myLibrary/mylibrary.h"

namespace Ui {
class registration;
}

class registration : public QDialog
{
    Q_OBJECT

public:
    explicit registration(Storage * storage, QWidget *parent = 0);
    ~registration();

    Storage * storage;

    QString randomNickname;

    bool isValid = false;

    int newUserId = 0;

private slots:
    void on_confirm_btn_clicked();

    void managerFinished(QNetworkReply *reply);

    void on_pushButton_clicked();

private:
    Ui::registration *ui;
    QNetworkAccessManager* manager;
    QNetworkRequest request;
};

#endif // REGISTRATION_H
